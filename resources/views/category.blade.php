@extends('layouts.template')

@section('content')

<div class="col-md-8">
	<div class="py-5">
		<a href="/categoryadd" class="btn-primary btn-lg float-right">Add</a>
	</div>
	<table class="table table-secondary table-hover table-bordered">
		<thead>
			<th>
				<tr>
				<td>
					No.
				</td>
				<td>
					Name
				</td>
				<td>Edit</td>
				<td>Delete</td>
				</tr>
				
			</th>
		</thead>
		@foreach($categories as $category)
		<tbody>
			
			<tr>
				
				<td>
					{{$category->id}}
				</td>

				<td>
					{{$category->category_name}}
				</td>
				<td>
					<a href="" class="btn btn-outline-success">EDIT</a>
				</td>
				<td>
					<a href="" class="btn btn-outline-danger">DELETE</a>
				</td>
				
			</tr>
		
		</tbody>
		@endforeach
	</table>
</div>





@endsection(content)

