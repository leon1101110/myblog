<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticationUsers;
use App\Http\Controllers\Controller;
use Auth;

class AdminLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function showLoginForm ()
    {
        return view('auth.adminLogin');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' > 'required|email',
            'password' => 'required|min:6'


        ]);


        if(Auth::guard('admin')->attempt(['email' => $request->email,'password' => $request->password],$request->remember)){

            return redirect()->intended('backend');
    }

    return back()->withErrors(['email' => 'Email or password are wrong.']);



}
}


