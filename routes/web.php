<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
    //return 'Hello Laraval';
});
*/

//Blog Home
Route::get('/','PostController@index');

//Blog Post
Route::get('post/{post}','PostController@show');

//Upload Post
Route::get('/upload','PostController@create');
Route::post('/upload','PostController@store');

//Delete Post
Route::get('/post/delete/{post}','PostController@destroy');
Route::get('/post/edit/{post}','PostController@edit');
Route::get('post/edit','PostController@update');

//Comments
Route::post('/comment','CommentController@store');

//category
Route::get('/category','CategoryController@index');
Route::get('/categoryadd','CategoryController@create');
Route::post('/categoryadd','CategoryController@store');



//Authentication scaffolding
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// for admin table
Route::get ('admin-login','Auth\AdminLoginController@showLoginForm');
Route::post ('admin-login','Auth\AdminLoginController@login');
Route::get ('backend','AdminController@index');

